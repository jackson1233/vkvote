var vm = new Vue({
	el: '#users',
	data: {
		first_user: {
			first_name: '',
			last_name: '',
			vk_id: '',
            avatar: '',
            marks: 0,
		},
		second_user: {
			first_name: '',
			last_name: '',
			vk_id: '',
            avatar: '',
            marks: 0,
		},
		group: ''
	},
	methods: {
		like: function(event) {
			var obj = (event.target)
			var f_id = obj.getAttribute('alt')

			if(obj.getAttribute('id') === 'first_user_img') {
                var s_id = this.second_user.vk_id
            } else {
				var s_id = this.first_user.vk_id
			}
			var formData = new FormData();
			formData.append('like', f_id)
			formData.append('dis', s_id)
			formData.append('group', this.group)
			this.$http.post('', formData).then(function (response) {
				var json = JSON.parse(response.data)
				this.new_data(json)
			}, function (error) {
				console.log(error)
				this.loading = false
			})
		},
		new_data: function (data) {
			var first_user = data[1].fields
            var second_user = data[2].fields

			this.group = data[0].group
			this.first_user.first_name = first_user.first_name
			this.first_user.last_name  = first_user.last_name
			this.first_user.avatar	   = first_user.avatar
			this.first_user.marks	   = first_user.marks
			this.first_user.vk_id	   = first_user.vk_id
			this.second_user.first_name = second_user.first_name
			this.second_user.last_name  = second_user.last_name
			this.second_user.avatar	    = second_user.avatar
			this.second_user.marks	    = second_user.marks
			this.second_user.vk_id	    = second_user.vk_id
		}
	},
	created: function () {
			this.$http.get('').then(function (response) {
				var json = JSON.parse(response.data)

				this.new_data(json)
			}, function (error) {
				console.log(error)
				this.loading = false
			})
    }
})
