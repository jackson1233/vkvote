from __future__ import absolute_import, unicode_literals
from vkgroup.models import GroupUser, Group
from celery import shared_task
from .helpers import get_users


@shared_task
def fill_group(group_id, title):
    group = Group.objects.get(pk=group_id)
    group.length, users = get_users(title)
    in_group_id = 1
    for user in users:
        tmp             = GroupUser()
        tmp.group       = group
        tmp.first_name  = user.get('first_name')
        tmp.last_name   = user.get('last_name')
        tmp.vk_id       = user['uid']
        tmp.in_group_id = in_group_id
        tmp.sex         = user.get('sex')
        tmp.avatar      = user.get('photo_200')
        tmp.save()
        in_group_id += 1
    group.save()
