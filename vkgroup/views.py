from __future__ import absolute_import, unicode_literals
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, ListView
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views import View
from django.core import serializers
from django.http import Http404

from random import sample

from .models import Group, GroupUser
from .forms import GroupCreationForm
from .tasks import fill_group


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    template_name = "vkgroup/create_group.html"
    form_class = GroupCreationForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        result = super(GroupCreateView, self).form_valid(form)
        fill_group.delay(self.object.pk, self.object.title)
        return result


class GroupUserListView(LoginRequiredMixin, ListView):
    model = GroupUser
    template_name = "vkgroup/top_users.html"
    context_object_name = "top_users"

    def get_queryset(self):
        slug = self.kwargs.get('slug')
        if slug == 'create':
            return []
        group = get_object_or_404(Group, slug=slug)
        users = group.users.order_by('-marks')[:10]
        return users


@method_decorator(csrf_exempt, name='dispatch')
class LikeView(LoginRequiredMixin, View):
    template_name = "vkgroup/like_user.html"

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            group       = get_object_or_404(Group, slug=kwargs.get('slug'))
            random_ids  = sample(range(1, group.length), 2)
            users       = GroupUser.objects.filter(in_group_id__in=random_ids)
            users_dict  = serializers.serialize('json',
                                                [users[0], users[1]],
                                                fields=('first_name',
                                                        'last_name',
                                                        'avatar',
                                                        'vk_id',
                                                        'marks',
                                                        ))
            users_dict = users_dict[0] + "{\"group\": " + group.slug + '}, ' + users_dict[1:]
            return JsonResponse(users_dict, status=200, safe=False)
        else:
            return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            l_user_id = request.POST.get('like')
            d_user_id = request.POST.get('dis')
            group = get_object_or_404(Group, slug=request.POST.get('group'))
            random_ids = sample(range(1, group.length), 2)
            users = GroupUser.objects.filter(in_group_id__in=random_ids)

            if l_user_id and d_user_id:
                l_user_id = int(l_user_id)
                d_user_id = int(d_user_id)
            else:
                raise Http404('Неправильный запрос')

            users_dict = serializers.serialize('json',
                                               [users[0], users[1]],
                                               fields=('first_name',
                                                       'last_name',
                                                       'avatar',
                                                       'vk_id',
                                                       'marks',
                                                       'group'))
            users_dict = users_dict[0] + "{\"group\": " + group.slug + '}, ' + users_dict[1:]
            users = GroupUser.objects.filter(vk_id__in=[l_user_id, d_user_id])
            like_user = users.get(vk_id=l_user_id)
            dislike_user = users.get(vk_id=d_user_id)
            like_user.marks += 1
            dislike_user.marks -= 1
            like_user.save()
            dislike_user.save()
            return JsonResponse(users_dict, status=200, safe=False)
        else:
            return render(request, self.template_name)