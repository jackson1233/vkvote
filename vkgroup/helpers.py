from django.conf import settings
import requests


def _clean_deleted_get_length(users):
    # Removing deactivated users and users WO photos
    active_user = users[:]
    for user in users:
        if ('deactivated' in user) or ('photo_200' not in user) or 'camera' in user.get('photo_200'):
            active_user.remove(user)

    return len(active_user), active_user


def get_users(group_id):
    users  = []
    fields = 'sex, photo_200'
    length = 0
    offset = 0

    while True:
        new_users = requests.get('https://api.vk.com/method/groups.getMembers?' +
                                 'access_token=' + settings.VK_TOKEN + '&' +
                                 'group_id=' + group_id + '&' +
                                 'offset=' + str(offset) + '&' +
                                 'fields=' + fields).json()['response']

        users += new_users['users']
        new_users_len = len(new_users['users'])
        offset += 1000

        # Break when reached the end of users list
        if new_users_len < 1000:
            break

    length, users = _clean_deleted_get_length(users)
    return length, users
