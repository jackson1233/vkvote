from django import forms

import requests

from .models import Group


class GroupCreationForm(forms.ModelForm):

    class Meta:
        model = Group
        fields = ['title']
        labels = {
            'title': 'Введите id группы'
        }

    def clean(self):
        result = super(GroupCreationForm, self).clean()
        title = self.cleaned_data['title']
        request = 'https://api.vk.com/method/groups.getById?group_id=' + title
        group = requests.get(request).json()
        not_active = group.get('error') or group.get('response')[0]['is_closed']

        if not_active:
            raise forms.ValidationError({'title': ['Такой группы не существует']})
        else:
            return result
