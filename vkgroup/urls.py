from django.urls import path

from . import views


app_name = "groups"

urlpatterns = [
    path('create/', views.GroupCreateView.as_view(), name="create"),
    path('<slug:slug>/', views.GroupUserListView.as_view(), name="detail"),
    path('<slug:slug>/vote/', views.LikeView.as_view(), name="vote"),
]
