from __future__ import absolute_import, unicode_literals
from django.db import models
from django.utils.text import slugify

from users.models import Profile
# Create your models here.


class Group(models.Model):
    title  = models.CharField(max_length=250, unique=True)
    slug   = models.SlugField(allow_unicode=True)
    length = models.IntegerField(default=0)

    class Meta:
        verbose_name        = "Group"
        verbose_name_plural = "Groups"

    def __str__(self):
        return self.title

    def natural_key(self):
        return self.slug

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super(Group, self).save(*args, **kwargs)


class GroupUser(models.Model):

    first_name  = models.CharField(max_length=250)
    last_name   = models.CharField(max_length=250)
    avatar      = models.URLField(max_length=250, blank=True)
    vk_id       = models.PositiveIntegerField(null=True)
    in_group_id = models.PositiveIntegerField(null=True)
    sex         = models.CharField(choices=[(1, 'Woman'), (2, 'Man')],
                                   max_length=50)
    marks       = models.IntegerField(default=0)
    group       = models.ForeignKey(Group,
                                    related_name='users',
                                    on_delete=models.CASCADE)

    class Meta:
        verbose_name        = "GroupUser"
        verbose_name_plural = "GroupUsers"

    def __str__(self):
        return '<{} {}:{}:{}>'.format(self.first_name,
                                      self.last_name,
                                      self.get_sex_display(),
                                      self.group)


class Likes(models.Model):
    liker = models.ForeignKey(Profile,
                              on_delete=models.CASCADE,
                              related_name='likes')
    likes = models.ForeignKey(GroupUser,
                              on_delete=models.CASCADE,
                              related_name='likers')
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return '<{}:{}:{}>'.format(self.liker,
                                   self.likes,
                                   self.date_created)
