from django.db import models
from django.utils import timezone
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)

# Create your models here.


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        username = self.model.normalize_username(username)
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        """
        Creates and saves a User with the given username and password.
        """
        extra_fields.set_default('is_staff', False)
        extra_fields.set_default('is_superuser', False)
        return self._create_user(self, username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        """
        Creates and saves a superuser with the given username and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, password, **extra_fields)


class AbstractCustomUser(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField('username',
                                max_length=150,
                                unique=True,
                                validators=[username_validator])
    is_staff = models.BooleanField('staff status', default=False)
    is_active = models.BooleanField('active', default=True,
        help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts.") 
    date_joined = models.DateTimeField('date joined', default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        abstract = True
        ordering = ['username']

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    @property
    def is_admin(self):
        return self.is_staff

    def __str__(self):
        return self.username


class Profile(AbstractCustomUser):
    class Meta(AbstractCustomUser.Meta):
        swappable = "AUTH_USER_MODEL"
        db_table = "users"
        verbose_name = "user"
        verbose_name_plural = "users"
