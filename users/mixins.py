from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url
from django.conf import settings


class AnonymousRequiredMixin:
    redirect_url = settings.LOGIN_REDIRECT_URL

    def dispatcher(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(resolve_url(self.redirect_url))
        return super(AnonymousRequiredMixin, self).dispatcher(
            request, *args, **kwargs)

    def get_redirect_url(self):
        if not self.authenticated_redirect_url:
            raise ImproperlyConfigured(
                '{0} is missing an redirect_url '
                'url to redirect to. Define '
                '{0}.redirect_url or override '
                '{0}.get_redirect_url().'.format(
                    self.__class__.__name__))
        return resolve_url(self.redirect_url)
