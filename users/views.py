from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate
from django.views.generic import CreateView, FormView
from django.urls import reverse_lazy

from .mixins import (AnonymousRequiredMixin,)
from .models import Profile
from .forms import UserCreationForm, LoginForm


class SignUpView(
        AnonymousRequiredMixin,
        CreateView,
):
    model = Profile
    form_class = UserCreationForm
    success_url = reverse_lazy('users:login')
    template_name = 'users/account_form.html'

    def get_context_data(self, **kwargs):
        kwargs.setdefault('form_action', 'Зарегистрироваться')
        return super(SignUpView, self).get_context_data(**kwargs)


class LoginView(
        AnonymousRequiredMixin,
        FormView,
):
    form_class = LoginForm
    success_url = reverse_lazy('home')
    template_name = 'users/account_form.html'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        kwargs.setdefault('form_action', 'Войти')
        return super(LoginView, self).get_context_data(**kwargs)


@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse_lazy('home'))
