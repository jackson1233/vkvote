from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from vkgroup.models import Group


@login_required
def index_view(request):
    groups = Group.objects.all()
    return render(request, 'index.html', context={'groups': groups})
